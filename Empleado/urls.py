from django.urls import path
from . import views

app_name = 'empleados'

urlpatterns = [
    path('', views.EmpleadoListView.as_view(), name='all'),
    path('empleado/<int:pk>/detail', views.EmpleadoDetailView.as_view(), name='empleado_detail'),
    path('empleado/create/', views.EmpleadoCreateView.as_view(), name='empleado_create'),
    path('empleado/<int:pk>/update/', views.EmpleadoUpdateView.as_view(), name='empleado_update'),
    path('empleado/<int:pk>/delete/', views.EmpleadoDeleteView.as_view(), name='empleado_delete'),
    path('empleado/reporte/general/', views.ReporteEmpleadosExcel.as_view(), name='reporte_general'),
    path('empleado/<int:pk>/details/', views.DetalleEmpleadoPDF.as_view(), name='details_general')
]
