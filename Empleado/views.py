from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import DetailView, ListView, DeleteView, TemplateView
from extra_views import InlineFormSetFactory, CreateWithInlinesView, UpdateWithInlinesView
from openpyxl.workbook import Workbook

from django.template.loader import get_template
from xhtml2pdf import pisa

from Direccion.models import Direccion
from Empleado.models import Empleado
from Telefono.models import Telefono


class TelefonoInline(InlineFormSetFactory):
    model = Telefono
    fields = ['numero', 'ext', 'tipo']
    prefix = 'telefono_form'
    factory_kwargs = {'extra': 1, 'max_num': None,
                      'can_order': False, 'can_delete': False}


class DireccionInline(InlineFormSetFactory):
    model = Direccion
    fields = ['entidad', 'calle', 'numero', 'cp']
    prefix = 'direccion_form'
    factory_kwargs = {'extra': 1, 'max_num': None,
                      'can_order': False, 'can_delete': False}


class EmpleadoBaseView(View):
    model = Empleado
    fields = ['nombre', 'apPaterno', 'apMaterno', 'edad', 'genero', 'puesto', 'grado', 'tipo']
    success_url = reverse_lazy('empleados:all')


class EmpleadoListView(EmpleadoBaseView, ListView):
    """View to list all Empleados.
    Use the 'Empleado_list' variable in the template
    to access all Empleado objects"""
    queryset = Empleado.objects.filter(activado=True)


class EmpleadoDetailView(EmpleadoBaseView, DetailView):
    """View to list the details from one Empleado.
    Use the 'Empleado' variable in the template to access
    the specific Empleado here and in the Views below"""


class EmpleadoCreateView(EmpleadoBaseView, CreateWithInlinesView):
    """View to create a new Empleado"""
    inlines = [DireccionInline, TelefonoInline]


class EmpleadoUpdateView(EmpleadoBaseView, UpdateWithInlinesView):
    """View to update a Empleado"""
    template_name = 'Empleado/empleado_form_update.html'
    inlines = [DireccionInline]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['telefonos'] = Telefono.objects.filter(empleado_id=context['pk'])

        return context


class EmpleadoDeleteView(EmpleadoBaseView, DeleteView):
    """View to delete a Empleado"""

    def form_valid(self, form):
        obj = self.get_object()
        obj.activado = False
        obj.save()

        return HttpResponseRedirect(self.get_success_url())


class ReporteEmpleadosExcel(TemplateView):

    def get(self, request, *args, **kwargs):
        empleados = Empleado.objects.filter(activado=True)

        wb = Workbook()
        ws = wb.active

        ws['B1'] = 'REPORTE GENERAL DE EMPLEADOS'
        ws.merge_cells('B1:E1')

        ws.column_dimensions['B'].width = 25
        ws.column_dimensions['C'].width = 25
        ws.column_dimensions['D'].width = 25
        ws.column_dimensions['E'].width = 10
        ws.column_dimensions['F'].width = 20
        ws.column_dimensions['G'].width = 25
        ws.column_dimensions['H'].width = 25
        ws.column_dimensions['I'].width = 25
        ws.column_dimensions['J'].width = 25
        ws.column_dimensions['K'].width = 25
        ws.column_dimensions['L'].width = 10
        ws.column_dimensions['M'].width = 10

        ws['B3'] = 'Nombre'
        ws['C3'] = 'Apellido Paterno'
        ws['D3'] = 'Apellido Materno'
        ws['E3'] = 'Edad'
        ws['F3'] = 'Genero'
        ws['G3'] = 'Puesto'
        ws['H3'] = 'Grado'
        ws['I3'] = 'Tipo Empleado'
        ws['J3'] = 'Entidad Federativa'
        ws['K3'] = 'Calle'
        ws['L3'] = 'Numero'
        ws['M3'] = 'Codigo Postal'
        cont = 4

        for empleado in empleados:
            ws.cell(row=cont, column=2).value = empleado.nombre
            ws.cell(row=cont, column=3).value = empleado.apPaterno
            ws.cell(row=cont, column=4).value = empleado.apMaterno
            ws.cell(row=cont, column=5).value = empleado.edad
            ws.cell(row=cont, column=6).value = empleado.genero
            ws.cell(row=cont, column=7).value = empleado.puesto
            ws.cell(row=cont, column=8).value = empleado.grado.grado
            ws.cell(row=cont, column=9).value = empleado.tipo.tipo
            ws.cell(row=cont, column=10).value = empleado.direccion.entidad.entidad
            ws.cell(row=cont, column=11).value = empleado.direccion.calle
            ws.cell(row=cont, column=12).value = empleado.direccion.numero
            ws.cell(row=cont, column=13).value = empleado.direccion.cp
            cont += 1

        archivo = "ReporteEmpleados.xlsx"

        response = HttpResponse(content_type="application/ms-excel")
        contenido = "attachment; filename={0}".format(archivo)
        response["Content-Disposition"] = contenido
        wb.save(response)

        return response


class DetalleEmpleadoPDF(TemplateView):

    def get(self, request, *args, **kwargs):
        empleado = get_object_or_404(Empleado, id=self.kwargs.get('pk'))

        context = {'empleado': empleado}
        html = render_to_string("Empleado/empleado_details.html", context)

        response = HttpResponse(content_type="application/pdf")
        response["Content-Disposition"] = "inline; report.pdf"

        pisa_status = pisa.CreatePDF(html, dest=response)

        if pisa_status.err:
            return HttpResponse("We had some error <pre>" + html + "</pre>")

        return response
