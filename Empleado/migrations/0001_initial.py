# Generated by Django 4.1.5 on 2023-01-04 20:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='GradoEstudioCatalogo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('grado', models.CharField(max_length=32, verbose_name='Grado de estudio catalogo')),
            ],
        ),
        migrations.CreateModel(
            name='TipoEmpleadoCatalogo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipo', models.CharField(max_length=32, verbose_name='Tipo de empleado catalogo')),
            ],
        ),
        migrations.CreateModel(
            name='Empleado',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=64, verbose_name='Nombre')),
                ('apPaterno', models.CharField(max_length=64, verbose_name='Apellido Paterno')),
                ('apMaterno', models.CharField(max_length=64, verbose_name='Apellido Materno')),
                ('edad', models.IntegerField(verbose_name='Edad')),
                ('genero', models.CharField(max_length=32, verbose_name='Genero')),
                ('puesto', models.CharField(max_length=64, verbose_name='Puesto')),
                ('activado', models.BooleanField(default=True)),
                ('grado', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='Empleado.gradoestudiocatalogo', verbose_name='Grado')),
                ('tipo', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='Empleado.tipoempleadocatalogo', verbose_name='Tipo de Empledo')),
            ],
        ),
    ]
