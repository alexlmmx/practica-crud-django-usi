from django.contrib import admin
from .models import Empleado, GradoEstudioCatalogo, TipoEmpleadoCatalogo

admin.site.register(Empleado)
admin.site.register(GradoEstudioCatalogo)
admin.site.register(TipoEmpleadoCatalogo)