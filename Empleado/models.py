from django.db import models


class GradoEstudioCatalogo(models.Model):
    grado = models.CharField(max_length=32, verbose_name="Grado de estudio catalogo")

    def __str__(self):
        return self.grado


class TipoEmpleadoCatalogo(models.Model):
    tipo = models.CharField(max_length=32, verbose_name="Tipo de empleado catalogo")

    def __str__(self):
        return self.tipo


class Empleado(models.Model):
    nombre = models.CharField(max_length=64, verbose_name='Nombre')
    apPaterno = models.CharField(max_length=64, verbose_name='Apellido Paterno')
    apMaterno = models.CharField(max_length=64, verbose_name='Apellido Materno')
    edad = models.IntegerField(verbose_name='Edad')
    genero = models.CharField(max_length=32, verbose_name='Genero')
    puesto = models.CharField(max_length=64, verbose_name='Puesto')

    grado = models.ForeignKey(GradoEstudioCatalogo, verbose_name='Grado', on_delete=models.DO_NOTHING)
    tipo = models.ForeignKey(TipoEmpleadoCatalogo, verbose_name='Tipo de Empledo', on_delete=models.DO_NOTHING)

    activado = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.nombre} {self.apPaterno} {self.apMaterno}'
