from django.db import models
from Empleado.models import Empleado


class EntidadCatalogo(models.Model):
    entidad = models.CharField(max_length=32, verbose_name='Entidad Federativa catalogo')

    def __str__(self):
        return f'{self.entidad}'


class Direccion(models.Model):
    entidad = models.ForeignKey(EntidadCatalogo, on_delete=models.DO_NOTHING, verbose_name='Entidad Federativa')
    calle = models.CharField(max_length=100, verbose_name='Calle')
    numero = models.PositiveIntegerField(verbose_name='Numero')
    cp = models.PositiveIntegerField(verbose_name='Codigo Postal')

    empleado = models.OneToOneField(
        Empleado,
        on_delete=models.DO_NOTHING,
        primary_key=True
    )

    def __str__(self):
        return f'{self.calle} {self.numero}'
