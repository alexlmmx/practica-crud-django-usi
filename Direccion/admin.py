from django.contrib import admin
from .models import Direccion, EntidadCatalogo

admin.site.register(Direccion)
admin.site.register(EntidadCatalogo)
