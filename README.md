## CRUD en Django

### Requisitos:
    - python 3.9 o superior
    - pip

Generar un entorno virtual o utilizar el global

### Instalar paquetes con pip

Se pueden instalar todos los paquetes necesarios ejecutando:
`pip install -r requirements.txt`

### Ejecutar el servidor de pruebas de django
`python3 manage.py runserver`


La base de datos se migro utilizando sqlite.
