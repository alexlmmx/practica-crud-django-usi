from django.shortcuts import render
from django.views.generic.edit import FormView
from .forms import EjercicioForm


def funcion_ejercicio(texto_string):
    min_consonantes = "bcdfghjklmnñpqrstvwxyz"
    may_consonates = min_consonantes.upper()
    consonantes = 0

    for i in range(len(texto_string)):
        if texto_string[i] in min_consonantes:
            consonantes += 1
            index = min_consonantes.index(texto_string[i]) + 1
            if index > len(min_consonantes):
                index = 0
            texto_string = texto_string[:i] + min_consonantes[index] + texto_string[i + 1:]
        elif texto_string[i] in may_consonates:
            consonantes += 1
            index = may_consonates.index(texto_string[i]) + 1
            if index > len(may_consonates):
                index = 0
            texto_string = texto_string[:i] + may_consonates[index] + texto_string[i + 1:]

    return consonantes, texto_string


def ejercicioView(request):
    consonantes = 0
    texto_editado = ""
    if request.method == "POST":
        form = EjercicioForm(request.POST)

        if form.is_valid():
            texto = form.cleaned_data['texto']
            consonantes, texto_editado = funcion_ejercicio(texto)
    form = EjercicioForm()
    return render(request, "Ejercicio/home.html", {"form": form, "consonantes": consonantes, "texto": texto_editado})


# if __name__ == "__main__":
#     funcion_ejericcio('hola mundo')
