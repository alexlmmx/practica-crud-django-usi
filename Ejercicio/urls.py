from django.urls import path
from . import views

app_name = 'ejercicio'

urlpatterns = [
    path('ejercicio/', views.ejercicioView, name='ejercicio')
]