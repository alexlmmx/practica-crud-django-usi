from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('Empleado.urls')),
    path('', include('Telefono.urls')),
    path('', include('Ejercicio.urls'))
]
