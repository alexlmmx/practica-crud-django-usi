from django.urls import path
from . import views

app_name = 'telefonos'

urlpatterns = [
    path('telefono/<int:pk>/create/', views.TelefonoCreateView.as_view(), name='telefono_create'),
    path('telefono/<int:pk>/update/', views.TelefonoUpdateView.as_view(), name='Telefono_update'),
    path('telefono/<int:pk>/delete/', views.TelefonoDeleteView.as_view(), name='Telefono_delete'),
]