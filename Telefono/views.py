from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy, reverse
from django.views import View
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from Empleado.models import Empleado
from .models import Telefono


class TelefonoBaseView(View):
    model = Telefono
    fields = ['numero', 'ext', 'tipo']
    success_url = reverse_lazy('empleados:all')


class TelefonoCreateView(TelefonoBaseView, CreateView):
    """View to create a new Telefono"""

    def form_valid(self, form):
        telefono = form.save(commit=False)
        telefono.empleado = get_object_or_404(Empleado, id=self.kwargs.get('pk'))

        return super(TelefonoCreateView, self).form_valid(form)

    def get_success_url(self):
        id = self.kwargs.get('pk')

        return reverse('empleados:empleado_update', kwargs={'pk': id})


class TelefonoUpdateView(TelefonoBaseView, UpdateView):
    """View to update a Telefono"""

    def get_success_url(self):
        obj = self.get_object()

        return reverse('empleados:empleado_update', kwargs={'pk': obj.empleado_id})


class TelefonoDeleteView(TelefonoBaseView, DeleteView):
    """View to delete a Telefono"""

    def form_valid(self, form):
        obj = self.get_object()
        id = obj.empleado_id
        obj.delete()

        return HttpResponseRedirect(
            reverse('empleados:empleado_update', kwargs={'pk': id}))