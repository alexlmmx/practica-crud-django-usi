from django.db import models
from Empleado.models import Empleado


class Telefono(models.Model):
    numero = models.CharField(max_length=20, verbose_name='Numero de telefono')
    ext = models.PositiveIntegerField(verbose_name='Extension')
    tipo = models.CharField(max_length=32, verbose_name='tipo de telefono')

    empleado = models.ForeignKey(Empleado, on_delete=models.DO_NOTHING, related_name='Telefonos')

    def __str__(self):
        return f'({self.ext}){self.numero}'
